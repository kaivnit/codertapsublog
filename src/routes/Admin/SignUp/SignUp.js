import React, {useState, useEffect} from 'react';
import UserService from '../../../services/business/UserService'
import {Link, withRouter} from "react-router-dom";
import Validation from '../../../ultils/Validation';
import {toast} from 'react-toastify';


const SignUp = ({history}) => {

    const checkEmail = (email) => {
        setTimeout(() => {
            UserService.checkExistEmail(email).then(res => {
                console.log(res, 'res ok')
            }).catch(err => {
                console.log(err, 'err not ok')
            })
        }, 900)
    };

    const checkUserName = (userName) => {
        setTimeout(() => {
            UserService.checkExistUserName(userName).then(res => {
                console.log(res);
            }).catch(err => {
                console.log(err)
            })
        }, 900)
    };

    const [form, setForm] = useState({
        firstName: {
            value: '',
            validate: [Validation.required],
            status: 'INVALID'
        },
        lastName: {
            value: '',
            validate: [Validation.required],
            status: 'INVALID'
        },
        email: {
            value: '',
            validate: [Validation.required, Validation.email],
            asyncValidate: checkEmail,
            status: 'INVALID'
        },
        userName: {
            value: '',
            validate: [Validation.required],
            asyncValidate: checkUserName,
            status: 'INVALID'
        },
        password: {
            value: '',
            validate: [Validation.required],
            status: 'INVALID'
        },
        gender: {
            value: '0',
            validate: [Validation.required],
            status: 'INVALID'
        }
    });

    const submitForm = (event) => {
        event.preventDefault();
        let userForm = {};
        Object.keys(form).forEach(key => {
            userForm[key] = form[key].value
        });
        UserService.signUp(userForm).then(res => {
            history.push('/admin/login');
            return toast.success(`${res.data.userName} has been successfully created.`);
        }).catch(err => {
            if (err && err.data) {
                return toast.error(err.data.message);
            } else {
                return toast.error(`Please try again later.`);
            }
        });
    };

    const validateForm = () => {
        Object.keys(form).forEach(key => {
            if (form[key].validate) {
                for (let i = 0, length = form[key].validate.length; i < length; i++) {
                    if (form[key].validate[i](form[key].value)) {
                        form[key].status = form[key].validate[i](form[key].value) ? 'INVALID' : 'VALID';
                    }
                }
                if (form[key].status === 'VALID' && !!form[key].asyncValidate) {
                    form[key].asyncValidate(form[key].value)
                }
            }
        });
    };

    useEffect(() => {
        // validateForm();
    }, [form]);


    return (
        <div className={'container'}>
            <div className={'box-wrapper mt-5'}>
                <div className="card">
                    <div className="card-body">
                        <h2 className="title text-center" style={{color: '#111111'}}>Registration</h2>
                        <form noValidate={true} onSubmit={(e) => submitForm(e)}>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="firstName" className="form-label">First Name</label>
                                    <input type="text" className="form-control" id="firstName" name="firstName"
                                           placeholder="First Name" defaultValue={form.firstName.value}
                                           onInput={(e) => setForm({...form, ...{firstName: {...form.firstName, ...{value: e.target.value}}}})}/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="lastName" className="form-label">Last Name</label>
                                    <input type="text" className="form-control" id="lastName" name="lastName"
                                           placeholder="Last Name" defaultValue={form.lastName.value}
                                           onInput={(e) => setForm({...form, ...{lastName: {...form.lastName, ...{value: e.target.value}}}})}/>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="email" className="form-label">Email</label>
                                    <input type="email" className="form-control" id="email" name="email"
                                           pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                           placeholder="Email" defaultValue={form.email.value}
                                           onInput={(e) => setForm({...form, ...{email: {...form.email, ...{value: e.target.value}}}})}/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="userName" className="form-label">User Name</label>
                                    <input type="text" className="form-control" id="userName" name="userName"
                                           placeholder="User Name" defaultValue={form.userName.value}
                                           onInput={(e) => setForm({...form, ...{userName: {...form.userName, ...{value: e.target.value}}}})}/>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="password" className="form-label">Password</label>
                                    <input type="password" className="form-control" id="password" name="password"
                                           placeholder="Password" defaultValue={form.password.value}
                                           onInput={(e) => setForm({...form, ...{password: {...form.password, ...{value: e.target.value}}}})}/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="gender" className="form-label">Gender</label>
                                    <select className="form-control" id="gender" defaultValue={form.gender.value}
                                            onChange={(e) => setForm({...form, ...{password: {...form.password, ...{value: e.target.value}}}})}>
                                        <option value={'0'}>Female</option>
                                        <option value={'1'}>Male</option>
                                        <option value={'2'}>Other</option>
                                    </select>
                                </div>
                            </div>
                            <div className={'row'}>
                                <button type="submit" className="m-0-auto px-5 btn btn-primary">Sign Up</button>
                            </div>
                            <div className={'row mt-2'}>
                                <Link to={`/admin/login`} className={'m-0-auto'}>
                                    <i className="fa fa-sign-in" aria-hidden="true"/>&nbsp;Back to login
                                </Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
};
export default withRouter(SignUp)

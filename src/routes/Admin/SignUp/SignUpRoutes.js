import SignUp from "./SignUp";

const SignUpRoutes = {
    path: '/register',
    exact: true,
    component: SignUp,
    auth: false,
    childRoutes: []
};
export default SignUpRoutes
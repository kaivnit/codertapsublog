import React, {useState} from 'react';
// import CKEditor from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Loading from "../../../components/Loading";
import {toast} from "react-toastify";
import MediaService from "../../../services/business/MediaService";

const Dashboard = () => {

    const [post, setPost] = useState({
        title: 'New post',
        topic: 'r',
        cover: '',
        section: [{
            heading: 'new heading',
            content: 'new content'
        }]
    });
    const addSection = () => {
        const sectionItem = {
            heading: '',
            content: ''
        };
        setPost({...post, ...{section: [...post.section, sectionItem]}});
    };

    const [hasSubHeading, setHasSubHeading] = useState(false);

    const [coverImg, setCoverimg] = useState(null);

    const uploadCoverImg = (e) => {
        e.persist();
        if (!(e.target.files && e.target.files.length > 0)) {
            return toast.error(`Please select at least one file`);
        }
        const file = e.target.files[0];
        MediaService.uploadOneImage(file).then(res => {
            e.target.value = null;
            setCoverimg(res.data)
        }).catch(err => {
            console.error(err);
            toast.error(`Upload failed. Please try again!`);
        })
    };

    const removeCoverImg = () => {
        MediaService.deleteImageById(coverImg._id).then(res => {
            setCoverimg(null);
        }).catch(err => {
            setCoverimg(null);
        });
    };

    return (
        <div className={'container'}>
            <div className={'editor-wrapper'}>
                {
                    post &&
                    <form action="">
                        <div className="form-row">
                            <div className="form-group col-md-9">
                                <label className={'form-label'} htmlFor={'postTitle'}>Post Title</label>
                                <input type="text" id={'posTitle'} className={'form-control'} defaultValue={post.title}
                                       placeholder={'Title'}/>
                            </div>
                            <div className="form-group col-md-3">
                                <label className={'form-label'} htmlFor={'postTopic'}>Choose Topic</label>
                                <select name="postTopic" id="postTopic" className={'form-control'}
                                        defaultValue={post.topic}>
                                    <option value={''}>Choose...</option>
                                    <option value={'a'}>Angular</option>
                                    <option value={'r'}>ReactJs</option>
                                    <option value={'n'}>NodeJs</option>
                                    <option value={'h'}>Html</option>
                                    <option value={'c'}>Css</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor={'cover-image'}
                                   className={'btn btn-outline-light btn-block py-3 cursor-pointer ' + (coverImg ? 'd-none' : '')}>
                                Add Cover Image&nbsp;<i className="fa fa-picture-o" aria-hidden="true"/>
                                <input type="file" id={'cover-image'} name={'coverImage'} accept={'image/*'}
                                       className={'d-none'} onChange={(e) => uploadCoverImg(e)}/>
                            </label>
                            {/*<img src={ coverImg && `${process.env.SERVER}${coverImg.path}`} className={'img-fluid w-100 cover-img ' + (coverImg ? '' : 'd-none')} alt=""/>*/}
                            <div className={'position-relative'}>
                                <span className={'position-absolute text-white remove-button cursor-pointer'}
                                      role={'button'} onClick={removeCoverImg}>&times;</span>
                                <img src={coverImg && `${process.env.SERVER}${coverImg.path}`}
                                     className={'img-fluid w-100 cover-img ' + (coverImg ? '' : 'd-none')}
                                     alt={'Cover Image'}/>
                            </div>
                        </div>
                        {
                            post.section && post.section.map((item, i) => (
                                <div key={i} className="post-body-section p-3 mt-3">
                                    <div className="form-row mb-3">
                                        <label className={'form-label m-auto col-md-2'}>
                                            <span className={hasSubHeading ? 'd-none' : ''}>Heading</span>
                                            <span
                                                className={'text-uppercase ' + (hasSubHeading ? '' : 'd-none')}>i.</span>
                                            <span
                                                className={'float-right cursor-pointer px-3 ' + (hasSubHeading ? 'd-none' : 'd-inline-block')}
                                                role={'button'}
                                                title={'Add sub-heading'} onClick={() => setHasSubHeading(true)}>
                                                <i className="fa fa-level-down" aria-hidden="true"/>
                                            </span>
                                        </label>
                                        <div className={'col-md-10'}>
                                            <input type="text" className={'form-control'} defaultValue={item.heading}/>
                                        </div>
                                    </div>
                                    <div className={'form-group ' + (hasSubHeading ? '' : 'd-none')}>
                                        <div className="form-row mb-3">
                                            <label className={'form-label col-md-2 m-auto text-uppercase'}>I.1
                                                <span className={'d-inline-block float-right cursor-pointer'}>
                                                    <span className={'px-2'} role={'button'}
                                                          title={'Remove sub-heading'}>
                                                        <i className="fa fa-minus" aria-hidden="true"/>
                                                    </span>
                                                    <span className={'px-2'} role={'button'} title={'Add sub-heading'}>
                                                        <i className="fa fa-plus" aria-hidden="true"/>
                                                    </span>
                                                 </span>
                                            </label>
                                            <div className={' col-md-10'}>
                                                <input type="text" className={'form-control'}/>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <label className={'form-label col-md-2 m-auto'}/>
                                            <div className={' col-md-10'}>
                                                <textarea rows="3" className={'form-control'}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={'form-group ' + (hasSubHeading ? 'd-none' : '')}>
                                        <label className={'form-label'}>Content</label>
                                        <textarea name="content" id="content" rows="3" className={'form-control'}
                                                  defaultValue={item.content}/>
                                    </div>
                                </div>
                            ))
                        }
                        <button type={'button'} className="btn btn-outline-light btn-block mt-3" onClick={addSection}>
                            Add more section
                        </button>
                    </form>
                }
            </div>
            <Loading/>
        </div>
    )
};
export default Dashboard;

import Dashboard from './Dashboard';

const DashboardRoutes = {
    path: '/dashboard',
    exact: false,
    component: Dashboard,
    auth: false,
    childRoutes: []
};
export default DashboardRoutes
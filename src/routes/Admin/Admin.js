import React from 'react';
import {Redirect, Route} from "react-router-dom";
import AdminRoutes from './AdminRoutes'

const Admin = ({match}) => {
    return (
        <div id={'admin-wrapper'}>
            {
                AdminRoutes && AdminRoutes.childRoutes.map((child, i) => (
                    <Route key={i} exact={child.exact} path={`${match.path}${child.path}`} component={child.component}/>
                ))
            }
            <Route exact path={match.path} render={() => (<Redirect to={`${match.path}/login`}/>)}/>
        </div>
    )
};
export default Admin
import Admin from "./Admin";
import SignInRoutes from "./SignIn/SignInRoutes";
import SignUpRoutes from "./SignUp/SignUpRoutes";
import DashboardRoutes from "./Dashboard/DashboardRoutes";

const AdminRoutes = {
    path: '/admin',
    exact: false,
    component: Admin,
    auth: false,
    childRoutes: [
        SignInRoutes,
        SignUpRoutes,
        DashboardRoutes
    ]
};
export default AdminRoutes
import React from 'react';
import UserService from '../../../services/business/UserService'
import {Link, withRouter} from "react-router-dom";
import {toast} from "react-toastify";

const {useState} = React;

const SignIn = () => {
    const [form, setForm] = useState({
        userName: '',
        password: ''
    });

    const submitForm = (event) => {
        event.preventDefault();
        UserService.signIn(form).then(res => {
            console.log(res)
        }).catch(err => {
            if (err && err.data) {
                return toast.error(err.data.message);
            } else {
                return toast.error(`Please try again later.`);
            }
        })
    };
    const loginSocial = (name) => {
        return toast.info('New feature coming soon');
        // switch (name) {
        //     case 'fb':
        //         console.log('fb');
        //         break;
        //     case  'gg':
        //         console.log('gg');
        //         break;
        //     case 'gl':
        //         console.log('gl');
        //         break;
        //     case 'gh':
        //         console.log('gh');
        //         break;
        // }
    };

    return (

        <div className={'container'}>
            <div className={'box-wrapper mt-5'}>
                <div className="card">
                    <div className="card-body">
                        <h2 className="title text-center" style={{color: '#111111'}}>Login</h2>
                        <form noValidate={true} onSubmit={(e) => submitForm(e)}>
                            <div className="form-group">
                                <label htmlFor="userName" className="form-label text-black">UserName or Email</label>
                                <input type="text" className="form-control" id="userName" name="userName"
                                       placeholder="User Name/Email" defaultValue={form.userName}
                                       onInput={(e) => setForm({...form, ...{userName: e.target.value}})}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" className="form-label text-black">Password</label>
                                <input type="password" className="form-control" id="password" name="password"
                                       placeholder="Password" defaultValue={form.password}
                                       onInput={(e) => setForm({...form, ...{password: e.target.value}})}/>
                            </div>

                            <div className={'row'}>
                                <button type="submit" className="m-0-auto px-5 btn btn-primary">Login</button>
                            </div>
                            <div className="row mt-2">
                                <Link to={`/admin/register`} className={'m-0-auto'}>
                                    <i className="fa fa-user-plus" aria-hidden="true"/>&nbsp;Create account
                                </Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div className={'mt-4'}>
                <h5 className="box-wrapper div-line-text text-uppercase">
                    or
                </h5>
            </div>
            <div className="mt-3 social-net">
                <div className="box-wrapper">
                    <div className="row">
                        <div className="col-sm-6">
                            <button type="button" className="btn btn-primary btn-block my-2"
                                    onClick={() => loginSocial('fb')}>
                                <i className="fa fa-facebook" aria-hidden="true"/>&nbsp;<span>Facebook</span>
                            </button>
                        </div>
                        <div className="col-sm-6">
                            <button type="button" className="btn btn-light btn-block my-2"
                                    onClick={() => loginSocial('gg')}>
                                <i className="fa fa-google" aria-hidden="true"/>&nbsp;<span>Google</span>
                            </button>
                        </div>
                        <div className="col-sm-6">
                            <button type="button" className="btn btn-warning btn-block my-2"
                                    onClick={() => loginSocial('gl')}>
                                <i className="fa fa-gitlab" aria-hidden="true"/>&nbsp;<span>Gitlab</span>
                            </button>
                        </div>
                        <div className="col-sm-6">
                            <button type="button" className="btn btn-dark btn-block my-2"
                                    onClick={() => loginSocial('gh')}>
                                <i className="fa fa-github" aria-hidden="true"/>&nbsp;<span>Github</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};
export default withRouter(SignIn)
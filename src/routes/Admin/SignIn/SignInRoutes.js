import SignIn from './SignIn';

const SignInRoutes = {
    path: '/login',
    exact: true,
    component: SignIn,
    auth: false,
    childRoutes: []
};
export default SignInRoutes
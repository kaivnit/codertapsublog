import AdminRoutes from './Admin/AdminRoutes';
import GuestRoutes from './Guest/GuestRoutes';
import CoreLayout from "./Core";

export default {
    path: '',
    component: CoreLayout,
    exact: true,
    auth: false,
    childRoutes: [
        GuestRoutes,
        AdminRoutes
    ]
}
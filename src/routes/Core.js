import React, {Fragment} from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Header from '../components/Header';
import RenderRoutes from '../ultils/RenderRoutes';
import {Redirect, Route, Switch} from "react-router-dom";
import NotFound from "../components/NotFound";

const CoreLayout = ({childRoutes}) => {
    return (
        <Fragment>
            <Header/>
            <main className={'main-body'}>
                <Switch>
                    {
                        childRoutes && childRoutes.map((child, i) => (
                            <RenderRoutes key={i} {...child} />
                        ))
                    }
                    <Route exact path={['/', '//', '///']} render={() => (<Redirect to="/home"/>)}/>
                    <Route component={NotFound}/>
                </Switch>
            </main>

            <ToastContainer hideProgressBar={true} autoClose={2000} />
        </Fragment>
    )
};
export default CoreLayout;
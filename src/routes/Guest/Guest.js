import React from 'react';
import PostService from '../../services/business/PostService';



const arr = Array.from(Array(10).keys());

const { useState, useEffect } = React;

const Guest = () => {
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        PostService.getPost().then(res => {
            setPosts(res)
        }).catch(err => console.log(err));
    }, []);

    return (
        <div>
            <div className="container">
                <div className="row">
                    {arr.map(i => (
                        <div key={i} className={'col-md-6 col-lg-4 p-0'}>
                            <a className={'d-inline-block my-3 mx-2 px-1'} href="javascript:void(0)">
                                <div className="card-box new-post">
                                    <img className={'img-fluid img-cover w-100 h-50'}
                                         src={require('../../assets/images/permissionFolder.png')} alt=""/>
                                    <div className={'w-100 h-50 d-flex align-items-center justify-content-center'}>
                                        <h3 className={'m-0 p-3 text-center heading-thumb-post'}>Lorem ipsum dolor
                                            sit.</h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
};

export default Guest
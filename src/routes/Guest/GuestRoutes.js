import Guest from "./Guest";

const GuestRoutes = {
    path: '/home',
    exact: false,
    component: Guest,
    auth: false,
    childRoutes: []
};
export default GuestRoutes
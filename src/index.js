import React from 'react'
import ReactDOM from 'react-dom'
import './index.css';
import App from './app'
import * as serviceWorker from './serviceWorker'

const MOUNT_NODE = document.getElementById('mount-node');
ReactDOM.render( <App /> , MOUNT_NODE)
serviceWorker.unregister();
class Validation {
    CHECK_ONLY_SPACE = /^\s+$/gm;
    CHECK_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/gi;
    CHECK_WEBSITE = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gi;
    CHECK_MODILE_PHONE = /^(([+]{1}[0-9]{2}|0)[0-9]{9})$/g;
    required = (value) => {
        return (!value || this.CHECK_ONLY_SPACE.test(value)) ? {required: true} : null;
    };
    email = (value) => {
        if (!value) {
            return {email: true}
        }
        return (this.CHECK_EMAIL.test(value)) ? null : {email: true}
    };
    website = (value) => {
        if (!value) {
            return {website: true}
        }
        return (this.CHECK_WEBSITE.test(value)) ? null : {website: true}
    };
    mobile = (value) => {
        if (!value) {
            return {mobile: true}
        }
        return (this.CHECK_MODILE_PHONE.test(value)) ? null : {mobile: true}
    };

}

export default new Validation();
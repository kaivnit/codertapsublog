import React from 'react';
import { Route } from 'react-router-dom';

const RenderRoutes = (route) => {
    return (
        <Route
            exact={route.exact}
            path={route.path}
            render={props => (
                <route.component {...props} childRoutes={route.childRoutes} />
            )}
        />
    );
};
export default RenderRoutes

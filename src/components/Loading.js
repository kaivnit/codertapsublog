import React from 'react';

const Loading = () => {
    return (
        <div id="loading-page" className={'row mt-5'}>
            <div className="lds-roller m-0-auto">
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
            </div>
        </div>
    )
};

export default Loading;
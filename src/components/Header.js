import React from 'react'
import {Link, NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header id={'navigation'} style={{position: 'fixed', width: '100%', zIndex: '100'}}>
            <nav className={'top-bar d-flex align-items-center px-4'} role={'navigation'}>
                <Link to={'/home'} className={'link-none-style'}><h4 className={'brand-text m-0 pr-4'}>CoderTapSu</h4></Link>
                <div className={'list-desktop flex-grow-1'}>
                    <ul className={'d-flex'}>
                        <li>
                            <NavLink exact to={'/home'} activeClassName={'link-active'}
                                     className={'d-block menu-item link-menu'}>All</NavLink>
                        </li>
                        <li>
                            <NavLink exact to={'/angular'} activeClassName={'link-active'}
                                     className={'d-block menu-item link-menu'}
                                     href="javascript:void(0)">Angular</NavLink>
                        </li>
                        <li>
                            <NavLink to={'/react'} activeClassName={'link-active'}
                                     className={'d-block menu-item link-menu'}
                                     href="javascript:void(0)">ReactJs</NavLink>
                        </li>
                        <li>
                            <NavLink to={'/javascript'} activeClassName={'link-active'}
                                     className={'d-block menu-item link-menu'}
                                     href="javascript:void(0)">Javascript</NavLink>
                        </li>
                        <li>
                            <NavLink to={'/html'} activeClassName={'link-active'}
                                     className={'d-block menu-item link-menu'}
                                     href="javascript:void(0)">Html</NavLink>
                        </li>
                        <li>
                            <NavLink to={'/css'} activeClassName={'link-active'}
                                     className={'d-block menu-item link-menu'}
                                     href="javascript:void(0)">Css</NavLink>
                        </li>
                    </ul>
                </div>
                <div className={'list-tablet flex-grow-1'}>
                    <div className={'d-flex flex-row-reverse'}>
                        <div className={'position-relative pl-4 py-2 drop-menu-wrapper'}>
                            <h6 className={'m-0 pr-3'}>Categories</h6>
                            <ul className={'position-absolute drop-menu pr-3 pt-3'}>
                                <li className={'text-right'}>
                                    <Link to={'/home'} className={'d-block py-2 pr-2 pl-3 drop-menu-item'}>All</Link>
                                </li>
                                <li className={'text-right'}>
                                    <Link to={'/admin'} className={'d-block py-2 pr-2 pl-3 drop-menu-item'}>Angular</Link>
                                </li>
                                <li className={'text-right'}>
                                    <Link to={'/react'} className={'d-block py-2 pr-2 pl-3 drop-menu-item'}>ReactJs</Link>
                                </li>
                                <li className={'text-right'}>
                                    <Link to={'/javascript'} className={'d-block py-2 pr-2 pl-3 drop-menu-item'}>Javascript</Link>
                                </li>
                                <li className={'text-right'}>
                                    <Link to={'/html'} className={'d-block py-2 pr-2 pl-3 drop-menu-item'}>Html</Link>
                                </li>
                                <li className={'text-right'}>
                                    <Link to={'/css'} className={'d-block py-2 pr-2 pl-3 drop-menu-item'}>Css</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={'d-flex align-items-center'}>
                    <input type={'search'} className={'w-100 search-box'}/>
                    <a href={process.env.HOME_PAGE} target={'_blank'} className={'pr-0 menu-item link-menu'} title={`FaceBook Page`}>
                        <i className="fa fa-facebook" aria-hidden="true"/>
                    </a>
                    <Link to={'/admin'} className={'pr-0 menu-item link-menu'} title={'Login to write article'}>
                        <i className="fa fa-pencil" aria-hidden="true"/>
                    </Link>
                </div>
            </nav>
        </header>
    )
};
export default Header
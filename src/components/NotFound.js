import React from 'react';
import {Link} from "react-router-dom";

const textStyle = {
    fontSize: '20vw'
};

const NotFound = () => (
    <div className="container text-center" style={{paddingTop: '150px'}}>
        <div className="brand">
            <h1 className="text-uppercase">CoderTapSu</h1>
        </div>
        <h1 className={'dou-line-before dou-line-after'} style={{...textStyle}}>404</h1>
        <p>Oops! The Page you requested was not found!</p>
        <Link to={'/home'} className="btn-outline"> Back to Home</Link>
    </div>

);

export default NotFound;
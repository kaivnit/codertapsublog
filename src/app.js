import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/global.scss';
import {BrowserRouter, Route} from "react-router-dom";
import routes from './routes/routes';

const App = () => (
    <BrowserRouter>
        <Route
            path={routes.path}
            exact={routes.exact}
            render={props => (
                <routes.component {...props} childRoutes={routes.childRoutes}/>
            )}
        />
    </BrowserRouter>

);
export default App;
import ApiService from "../common/ApiService";

class UserService {
    checkExistEmail = async (email) => {
        return await ApiService.post('/users/check-email', {email})
    };
    checkExistUserName = async (userName) => {
        return await ApiService.post('/users/check-username', {userName})
    };

    signUp = async (form) => {
        return await ApiService.post('/users', {...form});
    };
    signIn = async (form) => {
        return await ApiService.post('/users/login', {...form});
    }
}

export default new UserService();
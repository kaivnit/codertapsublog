import ApiService from "../common/ApiService";

class PostService {
    getPost = async () => {
        const res = await ApiService.get('/users');
        return await res.data.map(x => ({
            id: x._id,
            name: x.name,
            desc: x.desc
        }));
    }
}

export default new PostService();
import ApiService from "../common/ApiService";

class MediaService {
    uploadOneImage = async (file) => {
        const image = new FormData;
        image.append('image', file);
        return await ApiService.post('/media/image', image)
    }
    deleteImageById = async (id) => await ApiService.delelte(`/media/image/${id}`)
}

export default new MediaService();
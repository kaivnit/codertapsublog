import {toast} from 'react-toastify';
import axios from 'axios';

axios.defaults.baseURL = `${process.env.API}`;

axios.interceptors.request.use((config) => {
    if (navigator.onLine) {
        return config;
    } else {
        toast.error('Network lost connection!', {
            position: toast.POSITION.TOP_CENTER
        });
        throw new Error();
    }
}, (error) => {
    return Promise.reject(error);
});

class ApiService {

    static handleRes(res) {
        return res && res.data
    }

    static handleErr(err) {
        throw err &&
        err.response && {
            status: err.response.status,
            statusText: err.response.statusText,
            data: err.response.data
        };
    }

    get = (url, config = {}) => {
        return axios.get(url, config)
            .then(res => {
                return ApiService.handleRes(res)
            })
            .catch(err => ApiService.handleErr(err));
    };
    post = (url, body, config = {}) => {
        return axios.post(url, body, config)
            .then(res => ApiService.handleRes(res))
            .catch(err => ApiService.handleErr(err));
    };
    put = (url, config = {}) => {
        return axios.put(url, config)
            .then(res => ApiService.handleRes(res))
            .catch(err => ApiService.handleErr(err))
    };
    delelte = (url, config) => {
        return axios.delete(url, config)
            .then(res => ApiService.handleRes(res))
            .catch(err => ApiService.handleErr(err))
    };
}

export default new ApiService();
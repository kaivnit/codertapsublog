class LocalCache {
    setSession = (key, value) => window.sessionStorage.setItem(key, value);
    setMultiSession = (arr) => arr.map(x => window.sessionStorage.setItem(x.key, x.value));
    getSession = (key) => window.sessionStorage.getItem(key);
    deleteSession = (key) => window.sessionStorage.removeItem(key);
    deleteMultiSession = (arr) => arr.map(x => window.sessionStorage.removeItem(x));

    setLocal = (key, value) => window.localStorage.setItem(key, value);
    setMultiLocal = (arr) => arr.map(x => window.localStorage.setItem(x.key, x.value));
    getLocal = (key) => window.localStorage.getItem(key);
    deleteLocal = (key) => window.localStorage.removeItem(key);
    deleteMultiLocal = (arr) => arr.map(x => window.localStorage.removeItem(x));
}

export default new LocalCache();